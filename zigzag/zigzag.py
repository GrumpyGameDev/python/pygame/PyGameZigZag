import pygame
import random
from enum import Enum

class GameState(Enum):
    GAME_OVER = 0
    PLAYING = 1

RED = pygame.Color(255,0,0)
YELLOW = pygame.Color(255,255,0)
BLACK = pygame.Color(0,0,0)
WHITE = pygame.Color(255,255,255)
BLUE = pygame.Color(0,0,255)
GREEN = pygame.Color(0,255,0)

CELL_SIZE = CELL_WIDTH, CELL_HEIGHT = (16,16)
GRID_SIZE = GRID_COLUMNS, GRID_ROWS = (40,30)
TAIL_LENGTH = 6

def mulTuples(first,second):
    return (first[0] * second[0], first[1] * second[1])
def addTuples(first,second):
    return (first[0] + second[0], first[1] + second[1])
pygame.init()

SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT = mulTuples(CELL_SIZE,GRID_SIZE)
screen = pygame.display.set_mode(SCREEN_SIZE)
pygame.display.set_caption("ZigZag")

playing=True

clock=pygame.time.Clock()
font = pygame.font.SysFont("FreeMono",CELL_HEIGHT)

score = 0
blocks = []

def initblocks():
    global blocks
    blocks=[]        
    while len(blocks)<GRID_ROWS:
        blocks.append(0)

initblocks()

tail=[]
tailcolors=[]
while len(tailcolors)<(TAIL_LENGTH-1):
    tailcolors.append(YELLOW)
tailcolors.append(RED)

def inittail():
    global tail
    tail=[]
    while len(tail)<TAIL_LENGTH:
        tail.append(GRID_COLUMNS//2)
        
inittail()

direction = 1

runlength = 0

gamestate = GameState.GAME_OVER

def turn_left():
    global direction
    global score
    global runlength
    if direction != -1:
        score += (runlength) * (runlength+1)//2
        runlength=0
        direction = -1

def turn_right():
    global direction
    global score
    global runlength
    if direction!=1:
        score += (runlength) * (runlength+1)//2
        runlength=0
        direction = 1

def start_game():
    global direction
    global runlength
    global gamestate
    global score
    score=0
    inittail()
    initblocks()
    direction=1
    runlength=0
    gamestate = GameState.PLAYING

while playing:
    screen.fill(BLACK)
            
    row = 0
    for column in blocks:
        pygame.draw.rect(screen, WHITE,pygame.Rect((column*CELL_WIDTH, row * CELL_HEIGHT),CELL_SIZE))
        row += 1
        
    row = 0
    for column in tail:
        pygame.draw.rect(screen, tailcolors[row], pygame.Rect((column * CELL_WIDTH, row * CELL_HEIGHT), CELL_SIZE))
        row += 1
        
    pygame.draw.rect(screen,BLUE,pygame.Rect((0,0),(CELL_WIDTH,SCREEN_HEIGHT)))
    pygame.draw.rect(screen,BLUE,pygame.Rect((SCREEN_WIDTH-CELL_WIDTH,0),(CELL_WIDTH,SCREEN_HEIGHT)))
    
    if gamestate == GameState.PLAYING:
        runlength+=1
        tail.append(tail[len(tail)-1]+direction)
        tail.remove(tail[0])
        blocks.remove(blocks[0])
        blocks.append(random.randint(1,GRID_COLUMNS-2))
        if tail[len(tail)-1]==blocks[len(tail)-1] or tail[len(tail)-1]==0 or tail[len(tail)-1]==GRID_COLUMNS-1:
            gamestate = GameState.GAME_OVER
            
    fontSurface = font.render(str(score),False,GREEN)
    screen.blit(fontSurface,(CELL_WIDTH,0))

    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            playing=False
        elif event.type == pygame.KEYDOWN:
            if gamestate == GameState.GAME_OVER:
                if event.key == pygame.K_SPACE:
                    start_game()
            else:
                if event.key == pygame.K_LEFT:
                    turn_left()
                elif event.key == pygame.K_RIGHT:
                    turn_right()

    pygame.display.flip()
    clock.tick(10)


pygame.quit()